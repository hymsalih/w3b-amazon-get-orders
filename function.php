<?php

require_once dirname(__FILE__) . "/.config.inc.php";
set_time_limit(0);
require_once "ListOrdersClass.php";
require_once "ListOrderItemsClass.php";
require_once "ListOrdersByNextTokenClass.php";
require_once "GetOrderRequest.php";
$config = array(
    'ServiceURL' => SERVICE_URL,
    'ProxyHost' => null,
    'ProxyPort' => -1,
    'ProxyUsername' => null,
    'ProxyPassword' => null,
    'MaxErrorRetry' => 3,
);
$service = new MarketplaceWebServiceOrders_Client(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, APPLICATION_NAME, APPLICATION_VERSION, $config);
$noOfdays = 10;
$orderResponse = listOrder($noOfdays);
while (isset($orderResponse['response']) && $orderResponse['response'] == "Request is throttled") {
    sleep(60);
    $orderResponse = listOrder($noOfdays);
}
if (isset($orderResponse->ListOrdersResult->Orders->Order)) {
    $orders = $orderResponse->ListOrdersResult->Orders->Order;
    storeOrder($orders);
}
if (isset($orderResponse->ListOrdersResult->NextToken)) {
    $nextToken = $orderResponse->ListOrdersResult->NextToken;
    getByNextToken($nextToken);
}

function getByNextToken($nextToken)
{
    $orderResponse = listOrderByNextToken($nextToken);
    while (isset($orderResponse['response']) && $orderResponse['response'] == "Request is throttled") {
        sleep(60);
        $orderResponse = listOrderByNextToken($nextToken);
    }

    if (isset($orderResponse->ListOrdersByNextTokenResult->Orders->Order)) {
        $orders = $orderResponse->ListOrdersByNextTokenResult->Orders->Order;
        storeOrder($orders);
    }

    while ($orderResponse->ListOrdersByNextTokenResult->NextToken) {
        $nextToken = $orderResponse->ListOrdersByNextTokenResult->NextToken;
        $orderResponse = listOrderByNextToken($nextToken);

        while (isset($orderResponse['response']) && $orderResponse['response'] == "Request is throttled") {
            sleep(60);
            $orderResponse = listOrderByNextToken($nextToken);
        }

        if (isset($orderResponse->ListOrdersByNextTokenResult->Orders->Order)) {
            $orders = $orderResponse->ListOrdersByNextTokenResult->Orders->Order;
            storeOrder($orders);
        }
        sleep(6);
    }
}
