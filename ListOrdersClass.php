<?php
$config = array(
    'ServiceURL' => SERVICE_URL,
    'ProxyHost' => null,
    'ProxyPort' => -1,
    'ProxyUsername' => null,
    'ProxyPassword' => null,
    'MaxErrorRetry' => 3,
);

$service = new MarketplaceWebServiceOrders_Client(AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, APPLICATION_NAME, APPLICATION_VERSION, $config);

function invokeGetOrder(MarketplaceWebServiceOrders_Interface $service, $request) {
    try {
        $response = $service->GetOrder($request);
        $dom = new DOMDocument();
        $dom->loadXML($response->toXML());
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $responseXML = $dom->saveXML();
        $response = new SimpleXMLElement($responseXML);
        return $response;
    } catch (MarketplaceWebServiceOrders_Exception $ex) {
        
    }
}

function listOrder($noOfdays) {
    global $service;
    $request = new MarketplaceWebServiceOrders_Model_ListOrdersRequest();
    $request->setSellerId(MERCHANT_ID);
    $today = date('Y-m-d H:i:s');
    if ($noOfdays == 1) {
        $last15 = date('Y-m-d H:i:s', strtotime($today . ' - ' . $noOfdays . 'day'));
    } else {
        $last15 = date('Y-m-d H:i:s', strtotime($today . ' - ' . $noOfdays . 'days'));
    }
    $createdAfter = new DateTime($last15, new DateTimeZone('UTC'));
    $request->setCreatedAfter($createdAfter->format('Y-m-d'));
    $request->setMarketplaceId(array(MARKETPLACE_ID));
    $order = invokeListOrders($service, $request);
    return $order;
}

function invokeListOrders(MarketplaceWebServiceOrders_Interface $service, $request) {
    try {
        $response = $service->ListOrders($request);
        $dom = new DOMDocument();
        $dom->loadXML($response->toXML());
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $responseXML = $dom->saveXML();
        $response = new SimpleXMLElement($responseXML);
        return $response;
    } catch (MarketplaceWebServiceOrders_Exception $ex) {
        return array('response' => $ex->getMessage());
    }
}
