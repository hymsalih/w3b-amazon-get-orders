<?php

function invokeListOrderItems(MarketplaceWebServiceOrders_Interface $service, $request) {
    try {
        $response = $service->ListOrderItems($request);
        $dom = new DOMDocument();
        $dom->loadXML($response->toXML());
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $responseXML = $dom->saveXML();
        $response = new SimpleXMLElement($responseXML);
        return $response;
    } catch (MarketplaceWebServiceOrders_Exception $ex) {
        return array('response' => $ex->getMessage());
    }
}
